This app should be used when the distance between 2 coordinates needs to be calculated, but only if its not within the Moscow Ring.
The app to be opened in a browser and insert a valid address into the input field.
The app will process the addres with a geocoder api, and translate the address into coordinates.
We have the desired coordinates now the app will check if these points are within the Moscow Ring Road.
This is done by transforming the MKAD coordinates into a Polygon.
If the address' coordinates are within the Polygon it will not calculate the distance and will inform the user with a message " The address is within Moscow Ring Road"  
There is a button for "New Query", which will clear the message.
If the address is not within the Polygon (Moscow Ring), the app will check for the nearest coordinates of the Moscow Ring.
When these coordinates are returned a algorithm (haversine) is applied between the entered address' coordinates and the nearest point to the Ring to calculate the distance in kilometers.
After the calculation the following message will appear "The distance between the address you have entered and the closest point to Moscow Ring Road" " is (in km): "
The result is saved to the distance.log file.